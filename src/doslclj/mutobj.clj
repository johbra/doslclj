(ns doslclj.core)

(defmacro defobj
  [sym slots]
  `(def ~sym
     (let ~slots
       (letfn [(self# [message#]
                 (let [messidx#  (.indexOf '~slots message#)
                       value# (if (= messidx# -1)
                                (throw (Exception. (str "Message not understood: " message#)))
                                (nth ~slots messidx#))]
                   (if (fn? value#) (value#) value#)))]
         self#))))

;; james := (|firstName := ’James’. lastName :=’Nielsen’.
;;            age := 25 |).

(defobj james [first-name "James" last-name "Nielsen" age 25])

james  ;; #<core$eval7272$self__7215__auto____7273 doslclj.core$eval7272$self__7215__auto____7273@1e78a60e>

(james 'last-name) ;; => "Nielsen"
(james 'age)       ;; => 25
(james 'alter)     ;; Exception Message not understood: alter  ...)

;; Die Angabe einer Methode bei der Objekterzeugung erfolgt auf diese Weise:
;;    karl := (|vorName := ’Karl’. nachName := ’Liebknecht’ |
;;              vollName := [self vorName, ’ ’
;;                           , self nachName]).

(defobj karl [vorname "karl" nachname "lieb"
              vollname (fn [] (str vorname " " nachname))])
karl ;; => #<core$eval7317$self__7215__auto____7320
;; doslclj.core$eval7317$self__7215__auto____7320@7e2ef2be>

(karl 'nachname) ;; => "lieb"
(karl 'vollname) ;; => "karl lieb"

;; Welche Syntax für Setter? 
(karl 'nachname "knecht")    ;; => ArityException Wrong number of args (2) passed ...
(karl :nachname "knecht")    ;;         "
(karl '(nachname "knecht"))  ;; => Exception Message not understood: (nachname "knecht") ...

;; Objekterzeugung ohne def
(defmacro obj
  [slots]
  `(let ~slots
     (letfn [(self# [message#]
               (let [messidx#  (.indexOf '~slots message#)
                     value# (if (= messidx# -1)
                              (throw (Exception. (str "Message not understood: " message#)))
                              (nth ~slots messidx#))]
                 (if (fn? value#) (value#) value#)))]
       self#)))

(obj [vorname "karl" nachname "lieb"
      vollname (fn [] (str vorname " " nachname))])
;; => #<core$eval7732$self__7694__auto____7735
;;     doslclj.core$eval7732$self__7694__auto____7735@2eef1bf4>

(def karlo (obj [vorname "karl" nachname "lieb"
                 vollname (fn [] (str vorname " " nachname))]))
(karlo 'vollname) ;; => "karl lieb"
(macroexpand '(obj [vorname "karl" nachname "lieb"
                    vollname (fn [] (str vorname " " nachname))]))
