(ns doslclj.core)

(defn duplicated-slot-names [vec]
  (let [slotnames (take-nth 2 vec)]
    (not= (count (into #{} slotnames))
          (count slotnames))))

(defn new-obj [message arg form]
  ;; ersetzt in der obj-Form form den Wert des Slots message durch arg und liefert neue obj-Form
  (list 'obj
        (->> (assoc (apply hash-map (second form)) message arg)
             (vec)
             (apply concat)
             (vec))))

(defmacro caller-locals->hashmap []
  (->> (keys &env)
       (map (fn [k] [`(quote ~k) k])) (into {})))

(defmacro obj
  [slots]
  (if (duplicated-slot-names slots) (throw (Exception. "duplicated slotname"))
      (let [s# 'self form &form]
        `(let ~slots
           (let [slts# (caller-locals->hashmap)]
             (letfn
                 [(self# [message# & args#]
                    (let [value#
                          (cond
                            (= message# '~s#) '~form
                            (contains? slts# message#) (message# slts#)
                            :else (throw (Exception.
                                          (str "Message not understood: " message#))))]
                      (if (fn? value#) (apply value# args#)
                          (if (empty? args#)
                            value#
                            (eval (new-obj message# (first args#) '~form))))))]
               self#))))))

(defmacro defobj
  [sym slots]
  `(def ~sym (obj ~slots)))

(defmacro add-slot
  [object methodname function]
  (let [s# 'self
        o# `(~object '~s#)
        slots# `(second ~o#)
        newslots# `(conj (conj ~slots# '~methodname) '~function)]
    (let [slots# (eval newslots#)]
      `(obj ~slots#))))



;; james := (|firstName := ’James’. lastName :=’Nielsen’.
;;            age := 25 |).

(defobj james [first-name "James" last-name "Nielsen" age 25])
(def james (obj [first-name "James" last-name "Nielsen" age 25]))

(james 'self)  ;; => (doslclj.core/obj [first-name "James" last-name "Nielsen" age 25])

(james 'last-name) ;; => "Nielsen"
(james 'age)       ;; => 25
;;(james 'alter)     ;; Exception Message not understood: alter  ...)

;; Die Angabe einer Methode bei der Objekterzeugung erfolgt auf diese Weise:
;;    karl := (|vorName := ’Karl’. nachName := ’Liebknecht’ |
;;              vollName := [self vorName, ’ ’
;;                           , self nachName]).

(defobj karl [vorname "karl" nachname "lieb"
              vollname (fn [gruss] (str gruss " " vorname " " nachname))])
(karl 'self)
;; => (doslclj.core/obj
;; [vorname
;;  "karl"
;;  nachname
;;  "lieb"
;;  vollname
;;  (fn [gruss] (str vorname " " nachname))])

(karl 'nachname)         ;; => "lieb"
(karl 'vollname "hallo") ;; => "hallo karl lieb"

;; setter erzeugen neues Objekt
((karl 'nachname "knecht") 'vollname "Hey") ;; => "Hey karl knecht"
(karl 'nachname)                            ;; => "lieb"  karl heißt weiterhin lieb

;; Objekterzeugung ohne def

((obj [vorname "karl" nachname "lieb"
       vollname (fn [] (str vorname " " nachname))]) 'vollname)
;; => "karl lieb"

(def karlo (obj [vorname "karl" nachname "lieb"
                 vollname (fn [] (str vorname " " nachname))]))
(karlo 'vollname) ;; => "karl lieb"

(def k (obj [name "gustav"]))
(def k1 (obj [name "daniel" freund k]))
(k1 'self)         ;; => (obj [name "daniel" freund k])
(k1 'freund 'name) ;; => "gustav"

(def k (obj [name "gustav"]))
(def k (add-slot k gruss (fn [gruss] (str gruss " " name))))
(k 'self) ;;  => (doslclj.core/obj
;;     [name "gustav" gruss (fn [gruss] (str gruss " " name))])
(k 'gruss "Hallo") ;; => "Hallo gustav"

((add-slot k alter 50) 'self)
;; => (doslclj.core/obj
;;      [name "gustav" gruss (fn [gruss] (str gruss " " name)) alter 50])

((add-slot (obj [x 1 y 5]) s (fn [] (+ x y))) 's)

