# Eine DOSL mit Clojure #

Erzeugen klassenloser Objekte in Anlehnung an die mit Pharo/Helvetia
implementierte Direct Object Specification Language

## Stand der Implementierung ##
+ Aufwand bisher: ca. 40 LOC
+ Fähigkeiten:
    1. Erzeugen von Objekten mit
        a. Datenfeldern („Exemplarvariablen“)
        b. Funktionen („Methoden“)
	2. nachträgliches Hinzufügen von Datenfeldern oder Funktionen
    2. Getter und Setter für die Datenfelder stehen automatisch zur
    Verfügung
    3. Objekte sind intern Funktionen.
    4. Methoden werden im „message passing style“ aktiviert.

## Ein paar Nutzungsbeispiele ##

Ein Objekt kann mithilfe zweier Macros, `obj` und `defobj`, erzeugt
werden. Dabei bindet `defobj` das erzeugte Objekt an einen Bezeichner,
`obj` tut das nicht. Der Aufruf

~~~~~
(defobj james [first-name "James" last-name "Nielsen" age 25])
~~~~~

erzeugt ein Objekt mit den Slots `first-name`, `last-name` und `age` und
bindet es an den Bezeichner `james`. Das Gleiche kann man auch mit dem
Aufruf

~~~~~
(def james (obj [first-name "James" last-name "Nielsen" age 25]))
~~~~~

erreichen. In jedem Fall ist `james` eine Funktion, die die Nachricht `self`
versteht:

~~~~~
(james 'self)  
;; => (doslclj.core/obj [first-name "James" last-name "Nielsen"
;;                       age 25])
~~~~~

Die Nachricht liefert den Macro-Aufruf, der das Objekt erzeugt.

Weitere Nachrichtensendungen:

~~~~~
(james 'last-name) ;; => "Nielsen"
(james 'age)       ;; => 25
(james 'alter)     ;; Exception Message not understood: alter  ...)
~~~~~

### Spezifikation von Methoden ###

„Methoden-Slots“ werden als Clojure-Funktionen spezifiziert:

~~~~~
(defobj karl [vorname "karl" nachname "lieb"
              vollname (fn [gruss] (str gruss " " vorname " " nachname))])
(karl 'self)
;; => (doslclj.core/obj
;; [vorname
;;  "karl"
;;  nachname
;;  "lieb"
;;  vollname
;;  (fn [gruss] (str vorname " " nachname))])

(karl 'nachname)         ;; => "lieb"
(karl 'vollname "hallo") ;; => "hallo karl lieb"
~~~~~

### Benutzung von Setters ###

Die Anwendung eines Setters geschieht mit dem Slot-Namen als Nachricht
ergänzt um das Argument für den neuen Wert. Es wird aber immer ein
neues Objekt erzeugt:

~~~~~
((karl 'nachname "knecht") 'vollname "Hey") ;; => "Hey karl knecht"
(karl 'nachname)         ;; => "lieb"  karl heißt weiterhin lieb
~~~~~

Das an `karl` gebundene Objekt besitzt nach wie vor den Nachnamen `"lieb"`.
Es sei denn man schreibt

~~~~~
(def karl (karl 'nachname "knecht"))
(karl 'nachname)    ;; => "knecht"  karl heißt jetzt knecht
~~~~~

Durch die Anwendung der Setter erfolgt also **keine** Objekt-Mutation. Möglich wäre das auch, bin mir
aber nicht sicher, ob man das haben wollen soll.

### Objekte sind Funktionen ###

Da vom `obj`-Macro Clojure-Funktionen erzeugt werden, kann man diese
auch anwenden, ohne sie vorher an einen Bezeichner zu binden:

~~~~~
((obj [vorname "karl" nachname "lieb"
       vollname (fn [] (str vorname " " nachname))]) 'vollname)
;; => "karl lieb"
~~~~~

Es ist auch möglich, Objekte als Werte von Daten-Slots einzusetzen:

~~~~~
(def k (obj [name "gustav"]))
(def k1 (obj [name "daniel" freund k]))
(k1 'self)         ;; => (obj [name "daniel" freund k])
(k1 'freund 'name) ;; => "gustav"
~~~~~

### Hinzufügen von Slots ###

Hierfür steht das Macro `add-slot` zur Verfügung:

~~~~~
(def k (obj [name "gustav"]))
(def k (add-slot k gruss (fn [gruss] (str gruss " " name))))
(k 'self) ;;  => (doslclj.core/obj
          ;;     [name "gustav" gruss (fn [gruss] (str gruss " " name))])
(k 'gruss "Hallo") ;; => "Hallo gustav"
~~~~~

Ebenso für Daten-Slots:

~~~~~
((add-slot k alter 50) 'self)
;; => (doslclj.core/obj
;;      [name "gustav" gruss (fn [gruss] (str gruss " " name)) alter 50])
~~~~~

Auch `add-slot` liefert immer ein **neues** Objekt

## Schlussbemerkung ##

**Probleme**

1. Slot-Namen können mehrfach vergeben werden. Beim Zugriff wird
   irgendeiner genommen.
2. Die Smalltalk DOSL erlaubt auch die Abstraktion von  DOSL-Objekten
   zu Klassen. Könnte eventuell noch interessant sein. 
3. Weitere Probleme suche ich noch.
